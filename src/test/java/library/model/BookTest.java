package library.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookTest {
    @Test
    void shouldBeEqualWhenTwoBooksHaveSameNameAndAuthor() {
        final Book refactoring1 = new Book("Refactoring", "Martin Fowler");
        final Book refactoring2 = new Book("Refactoring", "Martin Fowler");

        assertEquals(refactoring1, refactoring2);
    }
}
